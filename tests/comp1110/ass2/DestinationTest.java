package comp1110.ass2;

import org.junit.Test;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by lucy on 16/10/15.
 */
public class DestinationTest {
    private static String[] pieces = {
            "080",
            "106",
            "100",
            /*"082122",
            "100038106",
            "125064070043", use for multi pieces*/
    };

    private static final String nook = "093D038D064E070C100D043D106A108F072A080A051D112F082B016C118D060D125B122D";
    private static ArrayList<Integer> z = Nook.NooksIntList(nook);


    @Test
    public void testInNook() {
        //ArrayList<Integer> p = new ArrayList<>();
        for (int j = 0; j < pieces.length; j++){
            assertTrue("Incorrectly include destination: " + Destination.createDestination(nook, pieces[j]) + " that is not in nooks", z.contains(Destination.createDestination(nook, pieces[j])));
        }
    }

    @Test
    public void testDuplicateswithStarts() {
        for (int k = 0; k < pieces.length; k++){
            assertFalse(Destination.createDestination(nook, pieces[k]) == Integer.parseInt(pieces[k]));
        }
    }
}
