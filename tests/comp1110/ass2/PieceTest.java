package comp1110.ass2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by lucy on 16/10/15.
 */
public class PieceTest {
    private static Random r = new Random();
    private static final String nook = "093D038D064E070C100D043D106A108F072A080A051D112F082B016C118D060D125B122D";
    private static final int player_no = r.nextInt(5)+1;
    private static String a = Pieces.createPieces(nook);
    private static String b = Pieces.createMultiPieces(nook, player_no);

    @Test
    public void testRightPieceLength() {
        assertTrue("Piece length not right", a.length()== 3);
        assertTrue("Pieces length not right", b.length() == 3 || b.length() == 6 || b.length() == 9 || b.length() == 12); ;
    }

    @Test
    public void testInNook() {
        ArrayList z = Nook.NooksIntList(nook);
        assertFalse("Incorrectly include piece: " + a + " that is not in nooks", !z.contains(Integer.parseInt(a)));
        ArrayList<Integer> p = new ArrayList<>();
        for (int i = 0; i < b.length(); i = i + 3) {
            p.add(Integer.parseInt(b.substring(i, i + 3)));
        }
        for (int j = 0; j < p.size(); j++){
            assertFalse("Incorrectly include piece: "+ p.get(j)+ " that is not in nooks", !z.contains(p.get(j)));
        }
    }

    @Test
    public void testDuplicates() {
        ArrayList<Integer> p = new ArrayList<>();
        for (int i = 0; i < b.length(); i = i + 3) {
            p.add(Integer.parseInt(b.substring(i, i + 3)));
        }
        for (Integer x : p){
            p.remove(x);
            assertFalse("Duplicate piece: "+ "x", p.contains(x));
            p.add(x);
        }
    }

}
