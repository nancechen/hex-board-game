package comp1110.ass2;

import java.util.*;

import static java.util.Arrays.asList;

/**
 * Created by Sining on 2015/10/13.
 * This class is mainly used for creating a fully connected board.
 * Methods used written by Sining Li.
 */
public class SetNook {
    private final ArrayList<Hexgon> finalNooks;

    public ArrayList<Hexgon> getFinalNooks() {
        return finalNooks;
    }

    static List<Integer> t1 = new ArrayList<>(asList(8, 20, 21, 38, 39, 40, 62, 63, 64, 65, 92, 93, 94, 95, 96));
    static List<Integer> t2 = new ArrayList<>(asList(10, 23, 24, 42, 43, 44, 67, 68, 69, 70, 98, 99, 100, 101, 102));
    static List<Integer> t3 = new ArrayList<>(asList(12, 26, 27, 46, 47, 48, 72, 73, 74, 75, 104, 105, 106, 107, 108));
    static List<Integer> t4 = new ArrayList<>(asList(14, 29, 30, 50, 51, 52, 77, 78, 79, 80, 110, 111, 112, 113, 114));
    static List<Integer> t5 = new ArrayList<>(asList(16, 32, 33, 54, 55, 56, 82, 83, 84, 85, 116, 117, 118, 119, 120));
    static List<Integer> t6 = new ArrayList<>(asList(18, 35, 36, 58, 59, 60, 87, 88, 89, 90, 122, 123, 124, 125, 126));
    static List<Character> Oriens = new ArrayList<>(asList('A','B','C','D','E','F'));

    static ArrayList<List<Integer>> shuffledpositions(){
        ArrayList<List<Integer>> shuffledPositions = new ArrayList<>();
        Collections.shuffle(t1);
        Collections.shuffle(t2);
        Collections.shuffle(t3);
        Collections.shuffle(t4);
        Collections.shuffle(t5);
        Collections.shuffle(t6);
        shuffledPositions.add(t1);
        shuffledPositions.add(t2);
        shuffledPositions.add(t6);
        shuffledPositions.add(t5);
        shuffledPositions.add(t3);
        shuffledPositions.add(t4);

        return shuffledPositions;
    }
    public SetNook(final String crannies){
        ArrayList<Hexgon> findedNooks = new ArrayList<>();
        ArrayList<Hexgon> findedConnectedNodes = new ArrayList<>();
        ArrayList<Hexgon> settledNodes = new ArrayList<>();
        ArrayList<Hexgon> availableList = new ArrayList<>();
        Random random = new Random();

        ArrayList<Hexgon> cranniesList = Cranny.getCrannies(crannies);
        if(!cranniesList.isEmpty()){
            availableList.addAll(cranniesList);
        }
        availableList.addAll(BoardBound.getBoardBounds(crannies));
        ArrayList<List<Integer>> shuffledPositions = shuffledpositions();
        int[] statementForEachTri = {1,1,1,1,1,1};

        for(int i = 0; i<shuffledPositions.size();i++){
            List<Integer> triangle = shuffledPositions.get(i);
            ArrayList<Hexgon> positioninsInOneTriangle = new ArrayList<>();
            if(findedNooks.isEmpty()){
                char orientation = Oriens.get(random.nextInt(Oriens.size()));
                Nook determinedNook = new Nook(triangle.get(0),orientation,Point.convertHex(triangle.get(0)));
                findedNooks.add(determinedNook);
                findedConnectedNodes.add(determinedNook);
                settledNodes.add(determinedNook);

                positioninsInOneTriangle.add(determinedNook);
                triangle.remove(triangle.get(0));
                for(Hexgon potentialNode:availableList){
                    if (isPositionAvailable(potentialNode.position,determinedNook,findedNooks)&&!isAngleBounded(potentialNode,determinedNook)){
                        findedConnectedNodes.add(potentialNode);
                    }
                }
                availableList.addAll(BarrierNeibour.neighboursforANook(determinedNook));
            }
            for(Integer potentialPosition:triangle){
                if(positioninsInOneTriangle.size()>=3){
                    break;
                }
                ArrayList<Hexgon> newConnectedhexgons = new ArrayList<>();
                ArrayList<Hexgon> filteredConnetedHexgons = new ArrayList<>();
                for(Hexgon possibleNode:findedConnectedNodes){
                    if(!isneibour(potentialPosition,positioninsInOneTriangle) && isPositionAvailable(potentialPosition,possibleNode,findedNooks)){
                        for(Character potentialOrien:Oriens){
                            Hexgon potentialNook = new Nook(potentialPosition,potentialOrien,Point.convertHex(potentialPosition));
                            if(!isAngleBounded(potentialNook, possibleNode)){

                                findedConnectedNodes.add(potentialNook);
                                findedNooks.add(potentialNook);
                                newConnectedhexgons.add(potentialNook);
                                positioninsInOneTriangle.add(potentialNook);
                                statementForEachTri[i] = positioninsInOneTriangle.size();

                                for(Hexgon nodetoNookB:availableList){
                                    if(!isAngleBounded(potentialNook, nodetoNookB)&&!findedConnectedNodes.contains(nodetoNookB)&&isPositionAvailable(nodetoNookB.position,potentialNook,findedNooks)){
                                        newConnectedhexgons.add(nodetoNookB);
                                    }
                                }
                                for(Hexgon nodeToBeRemoved: findedConnectedNodes){
                                    if(NodeToBeFiltered(nodeToBeRemoved, findedNooks)){
                                        filteredConnetedHexgons.add(nodeToBeRemoved);
                                    }
                                }
                                availableList.addAll(BarrierNeibour.neighboursforANook(potentialNook));
                                break;
                            }
                        }
                        break;
                    }
                }
                findedConnectedNodes.addAll(newConnectedhexgons);
                findedConnectedNodes.removeAll(filteredConnetedHexgons);
            }
        }


        for(int i = 0;i<statementForEachTri.length;i++){
            if(statementForEachTri[i]<3){
                List<Integer> triangle = shuffledPositions.get(i);
                for(Integer potentialPosition:triangle){
                    if(statementForEachTri[i]>=3){
                        break;
                    }
                    ArrayList<Hexgon> newConnectedhexgons = new ArrayList<>();
                    ArrayList<Hexgon> filteredConnetedHexgons = new ArrayList<>();
                    for(Hexgon possibleNode:findedConnectedNodes){
                        if(!isneibour(potentialPosition,findedConnectedNodes) && isPositionAvailable(potentialPosition,possibleNode,findedNooks)){
                            for(Character potentialOrien:Oriens){
                                Hexgon potentialNook = new Nook(potentialPosition,potentialOrien,Point.convertHex(potentialPosition));
                                if(!isAngleBounded(potentialNook, possibleNode)){
                                    newConnectedhexgons.add(potentialNook);
                                    findedNooks.add(potentialNook);
                                    statementForEachTri[i]++;
                                    for(Hexgon nodetoNookB:availableList){
                                        if(!isAngleBounded(potentialNook, nodetoNookB)&&!findedConnectedNodes.contains(nodetoNookB)&&isPositionAvailable(nodetoNookB.position,potentialNook,findedNooks)){
                                            newConnectedhexgons.add(nodetoNookB);
                                        }
                                    }
                                    for(Hexgon nodeToBeRemoved: findedConnectedNodes){
                                        if(NodeToBeFiltered(nodeToBeRemoved, findedNooks)){
                                            filteredConnetedHexgons.add(nodeToBeRemoved);
                                        }
                                    }
                                    availableList.addAll(BarrierNeibour.neighboursforANook(potentialNook));
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    findedConnectedNodes.addAll(newConnectedhexgons);
                    findedConnectedNodes.removeAll(filteredConnetedHexgons);
                }
            }
        }
        if(findedNooks.size()==18){
            findedNooks = findedNooks;
        }else{
            findedNooks = new SetNook(crannies).finalNooks;
        };
        this.finalNooks = findedNooks;
    }
    private static boolean NodeToBeFiltered(Hexgon connectedNode, ArrayList<Hexgon> findedNooks){
        ArrayList<Double> anglesbetweenNooks = new ArrayList<>();
        ArrayList<Double> anglesbetweenNookandNode = new ArrayList<>();
        for(int i = 0; i<findedNooks.size();i++){
            for(int j = 0; j<findedNooks.size();j++){
                anglesbetweenNooks.add(Point.getAngle(findedNooks.get(i).coordinate,findedNooks.get(j).coordinate));
            }
            anglesbetweenNookandNode.add(Point.getAngle(findedNooks.get(i).coordinate,connectedNode.coordinate));
        }

        for(int m = 0; m<anglesbetweenNooks.size();m++){
            for(int n = 0; n < anglesbetweenNookandNode.size();n++){
                if(anglesbetweenNooks.get(m) == anglesbetweenNookandNode.get(n)){
                    return true;
                }
            }
        }
        return false;
    }

     private static boolean isPositionAvailable(int potentialPosition,Hexgon Node,List<Hexgon> settledPositions){
         boolean statement = false;
        if(potentialPosition>=0) {
            Point first = Point.convertHex(potentialPosition);
            ArrayList<Double> availableanglesofB = Block.anglefilter(Block.findBoundary(Node));
            for (Double angle : availableanglesofB) {
                if (Math.round(Point.getAngle(Node.coordinate, first)) == angle) {
                    statement = true;
                }
            }
        }
         if(!settledPositions.isEmpty()){
             for(Hexgon settledPoint:settledPositions){
                 if(potentialPosition>=0){
                     double angleA = Math.round(Point.getAngle(Node.coordinate,Point.convertHex(potentialPosition)));
                     double angleB = Math.round(Point.getAngle(Node.coordinate, settledPoint.coordinate));
                     if(angleA==angleB){
                         statement = false;
                     }
                 }
             }
         }

        return statement;
    }
    public static String generateNookString(ArrayList<Hexgon> generatedNooks){
        ArrayList<String> settedNooks = new ArrayList<>();

        if(generatedNooks==null){
            return "NOOOOOOO";
        }
        for(Hexgon nook:generatedNooks){
            Nook a = new Nook(nook.position,nook.orientation);
            settedNooks.add(a.toString());
        }

        StringBuilder nookString = new StringBuilder();
        for(String x:settedNooks){
            nookString.append(x);
        }
        return nookString.toString();
    }

    public static boolean isAngleBounded(Hexgon hexgona, Hexgon hexgonb){
        ArrayList<Double> unavailableanglesofA = Block.boundedangle(Block.findBoundary(hexgona));
        ArrayList<Double> unavailableanglesofB = Block.boundedangle(Block.findBoundary(hexgonb));
        double angleAtoB = Math.round(Point.getAngle(hexgonb.coordinate, hexgona.coordinate));
        double angleBtoA = Math.round(Point.getAngle(hexgona.coordinate,hexgonb.coordinate));
        boolean statement = true;

        if(!unavailableanglesofA.contains(angleBtoA)&&!unavailableanglesofB.contains(angleAtoB)){
            statement = false;
        }

        return statement;
    }
    public static double getDistance(int a, int b){
        Point first = Point.convertHex(a);
        Point second = Point.convertHex(b);

        double Xsquare = (first.getX()-second.getX())*(first.getX()-second.getX());
        double Ysquare = (first.getY()-second.getY())*(first.getY()-second.getY());
        return Math.sqrt(Xsquare+Ysquare);
    }

    private static boolean isneibour(int a, ArrayList<Hexgon> list){
        for(Hexgon x:list){
            double distance = getDistance(a,x.position);
            if(Math.abs(distance-20*Math.sqrt(3))<=0.01){
                return true;
            }
        }
        return false;
    }

}
