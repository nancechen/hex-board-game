package comp1110.ass2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sining on 2015/9/28.
 * Abstract data type of Nook, Cranny, BoardBound and BarrierNeighbour.
 */
public class Hexgon {

    int position;
    char orientation;
    String x;//only used in setting crannies.
    Point coordinate;
    Block block;
    public Hexgon(int position, char orientation){
        this.position = position;
        this.orientation = orientation;
    }
    public Hexgon(int position){
        this.position = position;
    }
    public Hexgon(String x){
        this.x = x;
    }
    public Hexgon(int position, char orientation, Point coordinate){
        this.position = position;
        this.orientation = orientation;
        this.coordinate = coordinate;
    }

    public Hexgon(int position, Point coordinate){
        this.position = position;
        this.coordinate = coordinate;
    }

    @Override
    public String toString(){
        if(position >=100){
            return Integer.toString(position)+Character.toString(orientation);
        }else if(position <100 & position >=10){
            return "0"+ position + orientation;
        }
        return "00"+ position + orientation;
    }

    public static ArrayList<Hexgon> combine(ArrayList<Hexgon> list){
        ArrayList<Hexgon> combinedlist = new ArrayList<>();
        for(Hexgon x:list){
            if(x instanceof Nook){
                int p = x.position;
                char o = x.orientation;
                Point z = Point.convertHex(p);
                combinedlist.add(new Nook(p, o, z));
            }
            if(x instanceof Cranny){
                int p = x.position;
                combinedlist.add(new Cranny(p,Point.convertHex(p)));
            }
            if(x instanceof BoardBound){
                int p = x.position;
                combinedlist.add(new BoardBound(p,Point.convertHex(p)));
            }
        }
        return combinedlist;
    }

    public static Hexgon convertInt(String gameString, int input){
        Game hexgame = new Game(gameString);
        List<Hexgon> nooks = hexgame.getNooks();
        List<Hexgon> crannies = hexgame.getCrannies();
        List<Hexgon> boardBounds = hexgame.getBoardBounds();
        double l = 10*Math.sqrt(3);
        for(Hexgon hex:nooks){
            Integer HexPos = hex.position;
            if(HexPos==input){
                return hex;
            }
            if(SetNook.getDistance(hex.position, input)==l){
                Point coordinate = Point.convertHex(input);
                Double angle = Point.getAngle(hex.coordinate,coordinate);
                return new BarrierNeibour(input,BarrierNeibour.find_bindedorein(angle),coordinate);
            }
        }
        for(Hexgon hex:crannies){
            if(hex.position==input){
                return hex;
            }
        }
        for(Hexgon hex:boardBounds){
            if(hex.position == input){
                return hex;
            }
        }
        return new Position(input);
    }

}
