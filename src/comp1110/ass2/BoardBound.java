package comp1110.ass2;

import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * Created by Sining on 2015/9/28.
 * Methods contained created by Sining Li.
 */
public class BoardBound extends Hexgon {

    public BoardBound(int position){
        super(position);
    }
    public BoardBound(int position,Point coordinate){
        super(position,coordinate);
    }

    public static boolean checkSameSide(int to,int from){
        if (to>=169&&to<=177&&from>=169&&from<=177){
            return true;
        }
        else if (to>=177&&to<=185&&from>=177&&from<=185){
            return true;
        }
        else if (to>=185&&to<=193&&from>=185&&from<=193){
            return true;
        }else if (to>=193&&to<=201&&from>=193&&from<=201){
            return true;
        }else if (to>=201&&to<=209&&from>=201&&from<=209){
            return true;
        }
        else if (to>=209&&to<=216&&from>=209&&from<=216){
            return true;
        }
        else return false;
    }//edited by Nance

    public static Integer boardNo(int m){
        int[] edge = {169,177,185,193,201,209};
        if(m>169&&m<177){
            return 1;
        }
        if(m>177&&m<185){
            return 2;
        }
        if(m>185&&m<193){
            return 3;
        }
        if(m>193&&m<201){
            return 4;
        }
        if(m>201&&m<209){
            return 5;
        }
        if(m>=209&&m<=216){
            return 6;
        }
        for (int i = 0; i < edge.length; i++) {
            if (edge[i] == m) {
                return 0;
            }
        }
        return -1;
    }

    public static ArrayList<Hexgon> getBoardBounds(String crannies){
        ArrayList<Hexgon> boardBoundpositions = new ArrayList<>();
        ArrayList<Integer> cranniesPositions = Cranny.getCranniesAndNeighbourPositions(crannies);
        for(int i = 169;i<=216;i++){
            if(!cranniesPositions.contains(i)){
                boardBoundpositions.add(new BoardBound(i));
            }
        }
        return Hexgon.combine(boardBoundpositions);
    }

}
