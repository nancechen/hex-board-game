package comp1110.ass2;

import java.util.ArrayList;

/**
 * Created by Sining on 2015/10/11.
 * this class constructs a data type that's not a Nook or Cranny or BoardBound but still has a block with it.
 * Methods Contained created by Sining Li.
 */
public class BarrierNeibour extends Hexgon {
    public BarrierNeibour(int position, char orientation, Point coordinate) {
        super(position, orientation, coordinate);
    }
    public static char find_bindedorein(Double angle){
        if(angle == 0){
            return 'a';
        }
        if(angle == 60){
            return 'b';
        }
        if(angle == 120){
            return 'c';
        }
        if(angle == 180){
            return 'd';
        }
        if(angle == 240){
            return 'e';
        }
        if(angle == 300){
            return 'f';
        }
        return 'N';
    }
    //this method was used for finding the positions of hexgons next to a Nook.
    public static ArrayList<Hexgon> neighboursforANook(Hexgon barrier){
        ArrayList<Hexgon> neibours = new ArrayList<>();
        Block nookBlock = Block.findBoundary(barrier);
        ArrayList<Double> boundedangles = Block.boundedangle(nookBlock);
        double l = 20*Math.sqrt(3);

        for(Double angle:boundedangles){
            Point neibourCoord = new Point(barrier.coordinate.getX()-l*Math.cos(angle*Math.PI/180),barrier.coordinate.getY()+l*Math.sin(angle*Math.PI/180));
            Integer neighbourPos = Point.convertPoint(neibourCoord);
            if(neighbourPos!= -1){
                neibours.add(new BarrierNeibour(neighbourPos,BarrierNeibour.find_bindedorein(angle),neibourCoord));
            }
        }
        return neibours;
    }
    public String toString(){
        return ""+position+orientation;
    }
}
