package comp1110.ass2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sining on 2015/9/1.
 * The class contains the constructor of a Nook and the methods that can separate a NookString into a List that contains Nooks.
 */
public class Nook extends Hexgon {
    public static String nook;


    public Nook(int position, char orientation) {
        super(position, orientation);
    }

    public Nook(int position, char orientation, Point coordinate) {
        super(position, orientation, coordinate);
    }

    public static String createNooksIris(String crannies){
        SetNook game = new SetNook(crannies);
        ArrayList<Hexgon> nooks = game.getFinalNooks();
        String nookString = SetNook.generateNookString(nooks);
        if(HexGame.legitimateNooks(nookString)){

            return nookString;
        }else{
            nookString = SetNook.generateNookString(nooks);
        }
        return nookString;
    }

    public static ArrayList getNooks(String nooks) {
        ArrayList<Character> a = new ArrayList<>();
        for (String c : nooks.split("[0-9]")) {//http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
            if (c != null && c.length() > 0)
                a.add(c.charAt(0));
        }
        ArrayList<Integer> n = new ArrayList<>();
        for (String b : nooks.split("[A-Z]")) {//http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
            n.add(Integer.parseInt(b));
        }
        ArrayList<Nook> nooklist = new ArrayList<>();
        if (a.size() == n.size())
            for (int i = 0; i < a.size(); i++) {
                nooklist.add(new Nook(n.get(i), a.get(i)));
            }
        return nooklist;
    }

    //by Lucy
    //Nooks in string list but without "ABCD.."
    public static ArrayList<String> NookStringList(String n) {
        ArrayList<String> NookStringList = new ArrayList<String>();
        ArrayList<String> NooksIntListS = getNooks(n);

        for (Object i : NooksIntListS) {
            String a = i.toString().substring(0, 3);
            NookStringList.add(a);
        }
        return NookStringList;
    }

    //Nooks in int list
    public static ArrayList<Integer> NooksIntList(String n) {
        ArrayList<Integer> NooksIntList = new ArrayList<>();
        for (String i : NookStringList(n)) {
            int x = Integer.parseInt(i.substring(0, 3));
            NooksIntList.add(x);
        }
        return NooksIntList;
    }

    public static char getOrientation(int nook, String nooks) {
        List<String> nookList = new ArrayList<>();
        for (int i = 0; i < 72; i = i + 4) {
            nookList.add((nooks.substring(i, i + 4)));
        }//a list of 18 nooks with orientation

        for (int i = 0; i < 18; i++) {
            String singleNook = nookList.get(i);
            int nookNo = Integer.parseInt(singleNook.substring(0, 3));
            if (nook == nookNo && singleNook.contains("A")) {
                return 'A';
            }
            if (nook == nookNo && singleNook.contains("B")) {
                return 'B';
            }
            if (nook == nookNo && singleNook.contains("C")) {
                return 'C';
            }
            if (nook == nookNo && singleNook.contains("D")) {
                return 'D';
            }
            if (nook == nookNo && singleNook.contains("E")) {
                return 'E';
            }
            if (nook == nookNo && singleNook.contains("F")) {
                return 'F';
            }
        }
        return 'Z';
    }//edited by Nance


    /**
     * Created by Sining on 2015/9/21.
     * Methods used in legitimateNook, just for the clarity in Hexgame, the methods are distracted.
     * created by the whole group.
     */
    public static class Nookmethods {

        public static List<Integer> get_position(List<Integer> list) {
            List<Integer> positions = new ArrayList<Integer>();
            for (int i = 0; i < 3; i++) {
                if (list.get(i) > 7 && list.get(i) <= 18) {
                    positions.add(1);
                }
                if (list.get(i) >= 19 && list.get(i) <= 36) {
                    positions.add(2);
                }
                if (list.get(i) >= 37 && list.get(i) <= 60) {
                    positions.add(3);
                }
                if (list.get(i) >= 61 && list.get(i) <= 90) {
                    positions.add(4);
                }
                if (list.get(i) >= 91 && list.get(i) <= 216) {
                    positions.add(5);
                }
            }
            return positions;
        }


        public static Boolean method_a(List<Integer> list,List<Integer> positions) {
            Boolean result = true;
            if ((Math.abs(positions.get(0) - positions.get(1)) > 1)
                    && (Math.abs(positions.get(0) - positions.get(2)) > 1)
                    && (Math.abs(positions.get(1) - positions.get(2)) > 1)) {
                result = true;
            } else {
                int a = list.get(0);
                int b = list.get(1);
                int c = list.get(2);
                if (Math.abs(positions.get(0) - positions.get(1)) <= 1 && result) {
                    if (a == b || a == (b + 1) || a == (b - 1) || ((Math.abs(b - a) - 1) % 6) == 0 && (Math.abs(b - a) - 1) != 0 || (b - a) % 6 == 0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(0) - positions.get(2)) <= 1 && result) {
                    if (a == c || a == (c + 1) || a == (c - 1) || ((Math.abs(c - a) - 1) % 6) == 0 && (Math.abs(c - a) - 1) != 0 || (c - a) % 6 == 0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(1) - positions.get(2)) <= 1 && result) {
                    if (c == b || c == (b + 1) || c == (b - 1) || ((Math.abs(b - c) - 1) % 6) == 0 && (Math.abs(b - c) - 1) != 0 || ((b - c) % 6 == 0)) {
                        result = false;
                    } else
                        result = true;
                }
            }
            return result;
        }

        public static Boolean method_b(List<Integer> list,List<Integer> positions) {
            Boolean result = true;
            if ((Math.abs(positions.get(0) - positions.get(1)) > 1)
                    && (Math.abs(positions.get(0) - positions.get(2)) > 1)
                    && (Math.abs(positions.get(1) - positions.get(2)) > 1)){
                result = true;
            } else {
                int a = list.get(0);
                int b = list.get(1);
                int c = list.get(2);
                if (Math.abs(positions.get(0) - positions.get(1)) <= 1 && result) {
                    if (a == b || a == (b + 1) || a == (b - 1) || ((Math.abs(b - a) - 2) % 6) == 0 && (Math.abs(b - a) - 2) != 0
                            || ((Math.abs(b - a) - 1) % 6) == 0 && (Math.abs(b - a) - 1) != 0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(0) - positions.get(2)) <= 1 && result) {
                    if (a == c || a == (c + 1) || a == (c - 1) || ((Math.abs(c - a) - 2) % 6) == 0 && (Math.abs(c - a) - 2) != 0
                            || ((Math.abs(c - a) - 1) % 6) == 0 && (Math.abs(c - a) - 1) != 0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(1) - positions.get(2)) <= 1 && result) {
                    if (c == b || c == (b + 1) || c == (b - 1) || ((Math.abs(b - c) - 2) % 6) == 0 && (Math.abs(b - c) - 2) != 0
                            || ((Math.abs(b - c) - 1) % 6) == 0 && (Math.abs(b - c) - 1) != 0) {
                        result = false;
                    } else
                        result = true;
                }
            }
            return result;
        }
        public static Boolean method_c(List<Integer> list,List<Integer> positions){
            Boolean result = true;
            if ((Math.abs(positions.get(0) - positions.get(1)) > 1)
                    && (Math.abs(positions.get(0) - positions.get(2)) > 1)
                    && (Math.abs(positions.get(1) - positions.get(2)) > 1)){
                result = true;
            } else {
                int a = list.get(0);
                int b = list.get(1);
                int c = list.get(2);
                if (Math.abs(positions.get(0) - positions.get(1)) <= 1 && result) {
                    if (a == b || a == (b + 1) || a == (b - 1) || ((Math.abs(b - a) - 2) % 6) == 0 && (Math.abs(b - a) - 2 !=0
                            || ((Math.abs(b - a) - 3) % 6) == 0 && (Math.abs(b - a) - 3) !=0)) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(0) - positions.get(2)) <= 1 && result) {
                    if (a == c || a == (c + 1) || a == (c - 1) || ((Math.abs(c - a) - 2) % 6) == 0 && (Math.abs(c - a) - 2) != 0
                            || ((Math.abs(c - a) - 3) % 6) == 0 && (Math.abs(c - a) - 3) != 0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(1) - positions.get(2)) <= 1 && result) {
                    if (c == b ||c == (b + 1) || c == (b - 1) || ((Math.abs(b - c) - 2) % 6) == 0 && (Math.abs(b - c) - 2) != 0
                            || ((Math.abs(b - c) - 3) % 6) == 0 && (Math.abs(b - c) - 3) != 0) {
                        result = false;
                    } else
                        result = true;
                }
            }
            return result;
        }
        public static Boolean method_d(List<Integer> list,List<Integer> positions){
            Boolean result = true;
            if ((Math.abs(positions.get(0) - positions.get(1)) > 1)
                    && (Math.abs(positions.get(0) - positions.get(2)) > 1)
                    && (Math.abs(positions.get(1) - positions.get(2)) > 1)){
                result = true;
            } else {
                int a = list.get(0);
                int b = list.get(1);
                int c = list.get(2);
                if (Math.abs(positions.get(0) - positions.get(1)) <= 1 && result) {
                    if (a == b || a == (b + 1) || a == (b - 1) || ((Math.abs(b - a) + 2) % 6) == 0 && (Math.abs(b - a) + 2) != 0
                            || ((Math.abs(b - a) + 3) % 6) == 0 && (Math.abs(b - a) + 3) !=0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(0) - positions.get(2)) <= 1 && result) {
                    if (a == c || a == (c + 1) || a == (c - 1) || ((Math.abs(c - a) + 2) % 6) == 0 && (Math.abs(c - a) + 2) != 0
                            || ((Math.abs(c - a) + 3) % 6) == 0 && (Math.abs(c - a) + 3) !=0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(1) - positions.get(2)) <= 1 && result) {
                    if (c == b || c == (b + 1) || c == (b - 1) || ((Math.abs(b - c) + 2) % 6) == 0 && (Math.abs(b - c) + 2) != 0
                            || ((Math.abs(b - c) + 3) % 6) == 0 && (Math.abs(b - c) + 3) !=0) {
                        result = false;
                    } else
                        result = true;
                }
            }
            return result;
        }
        public static Boolean method_e(List<Integer> list,List<Integer> positions){
            Boolean result = true;
            if ((Math.abs(positions.get(0) - positions.get(1)) > 1)
                    && (Math.abs(positions.get(0) - positions.get(2)) > 1)
                    && (Math.abs(positions.get(1) - positions.get(2)) > 1)){
                result = true;
            } else {
                int a = list.get(0);
                int b = list.get(1);
                int c = list.get(2);
                if (Math.abs(positions.get(0) - positions.get(1)) <= 1 && result) {
                    if (a == b || a == (b + 1) || a == (b - 1) || ((Math.abs(b - a) + 2) % 6) == 0 && (Math.abs(b - a) + 2) !=0
                            || ((Math.abs(b - a) + 1) % 6) == 0 && (Math.abs(b - a) + 1) != 0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(0) - positions.get(2)) <= 1 && result) {
                    if (a == c || a == (c + 1) || a == (c - 1) || ((Math.abs(c - a) + 2) % 6) == 0 && (Math.abs(c - a) + 2) !=0
                            || ((Math.abs(c - a) + 1) % 6) == 0 && (Math.abs(c - a) + 1) != 0) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(1) - positions.get(2)) <= 1 && result) {
                    if (c == b || c == (b + 1) || c == (b - 1) || ((Math.abs(b - c) + 2) % 6) == 0 && (Math.abs(b - c) + 2) !=0
                            || ((Math.abs(b - c) + 1) % 6) == 0 && (Math.abs(b - c) + 1) != 0) {
                        result = false;
                    } else
                        result = true;
                }
            }
            return result;
        }

        public static Boolean method_f(List<Integer> list,List<Integer> positions) {
            Boolean result = true;
            if ((Math.abs(positions.get(0) - positions.get(1)) > 1)
                    && (Math.abs(positions.get(0) - positions.get(2)) > 1)
                    && (Math.abs(positions.get(1) - positions.get(2)) > 1)) {
                result = true;
            } else {
                int a = list.get(0);
                int b = list.get(1);
                int c = list.get(2);
                if (Math.abs(positions.get(0) - positions.get(1)) <= 1 && result) {
                    if (a == b || a == (b + 1) || a == (b - 1) || ((Math.abs(b - a) + 1) % 6) == 0 && (Math.abs(b - a) + 1) != 0 || ((b - a) % 6 == 0)) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(0) - positions.get(2)) <= 1 && result) {
                    if (a == c || a == (c + 1) || a == (c - 1) || ((Math.abs(c - a) + 1) % 6) == 0 && (Math.abs(c - a) + 1) != 0 || ((c - a) % 6 == 0)) {
                        result = false;
                    } else
                        result = true;
                }
                if (Math.abs(positions.get(1) - positions.get(2)) <= 1 && result) {
                    if (c == b || c == (b + 1) || c == (b - 1) || ((Math.abs(b - c) + 1) % 6) == 0 && (Math.abs(b - c) + 1) != 0 || ((b - c) % 6 == 0)) {
                        result = false;
                    } else
                        result = true;
                }
            }
            return result;
        }
    }
}
