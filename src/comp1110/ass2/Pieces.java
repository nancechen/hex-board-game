package comp1110.ass2;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
//edited by Nance and Lucy
public class Pieces {
    public static String position;
    private static List<Integer> piecePositions = new ArrayList<>();

    public static String createPieces(String nooks) {
        List<Integer> nook = new ArrayList<>();

        for (String b : nooks.split("[A-Z]")) {
            nook.add(Integer.parseInt(b));

        }
        Random r = new Random();

        int i = r.nextInt(18);
        int p = nook.get(i);
        if (p < 100) {
            position = "0" + p;
        } else
            position = "" + p;
        return position;

    }//create single piece

    public static String createMultiPieces(String nooks, int numberPlayers) {
        System.out.println(nooks);
        List<Integer> nook = new ArrayList<>();

        for (String b : nooks.split("[A-Z]")) {
            nook.add(Integer.parseInt(b));
        }

        Random r = new Random();
        int a =18;
        for (int j = 0; j < numberPlayers; j++) {
            int i = r.nextInt(a);
            int p = nook.get(i);

            nook.remove(i);
            a--;
            if (p < 100) {
                position = "0" + Integer.toString(p);
            } else
                position = Integer.toString(p);
        }
        return position;
    }//create multiple pieces



     //belowings are for updating pieces position when moving mouse in javafx
    public static void addPiecePosition(int position) {
        piecePositions.add(position);
    }

    public static void updatePiecePosition(int piece, int position) {
        piecePositions.set(piece, position);
    }

    public static int getPiecePosition(int piece) {
        return piecePositions.get(piece);
    }

    public static List<Integer> getPiecePositions() {
        return piecePositions;
    }

}
