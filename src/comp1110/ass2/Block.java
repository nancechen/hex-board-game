package comp1110.ass2;

import java.util.*;

/**
 * Created by Sining on 2015/9/28.
 * This class is used for finding the block of each Hexgon, this Hexgon can be Nook, Cranny, BoardBound or the neighbour of a nook or cranny
 * Methods contained created by Sining Li. .
 */
public class Block {
    boolean a;
    boolean b;
    boolean c;
    boolean d;
    boolean e;
    boolean f;
    private static List<Integer> boundryList = new ArrayList<>();
    static {
        for (int i = 169; i <= 216; i++) {
            boundryList.add(i);
        }
    }//edited by Nance

    public Block(boolean a, boolean b, boolean c, boolean d, boolean e, boolean f){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }

    public static Block findBoundary(Hexgon x) {
        boolean a = false;
        boolean b = false;
        boolean c = false;
        boolean d = false;
        boolean e = false;
        boolean f = false;
        Block s = new Block(a, b, c, d, e, f);

        if (x instanceof Nook) {
            char q = ((Nook) x).orientation;
            switch (q) {
                case 'A':
                    s.a = true;
                    s.b = true;
                    s.c = true;
                    break;
                case 'B':
                    s.b = true;
                    s.c = true;
                    s.d = true;
                    break;
                case 'C':
                    s.c = true;
                    s.d = true;
                    s.e = true;
                    break;
                case 'D':
                    s.d = true;
                    s.e = true;
                    s.f = true;
                    break;
                case 'E':
                    s.e = true;
                    s.f = true;
                    s.a = true;
                    break;
                case 'F':
                    s.a = true;
                    s.b = true;
                    s.f = true;
                    break;
            }
        }
        if(x instanceof BarrierNeibour){
            switch (x.orientation){
                case'a':
                    s.a = true;
                    break;
                case 'b':
                    s.b = true;
                    break;
                case 'c':
                    s.c = true;
                    break;
                case 'd':
                    s.d = true;
                    break;
                case 'e':
                    s.e = true;
                    break;
                case 'f':
                    s.f = true;
                    break;
            }
        }
        if (x instanceof Cranny) {
            switch (BoardBound.boardNo(((Cranny) x).position)) {
                case 1:
                    s.b = true;
                    s.c = true;
                    s.d = true;
                    break;
                case 2:
                    s.d = true;
                    s.c = true;
                    s.e = true;
                    break;
                case 3:
                    s.d = true;
                    s.e = true;
                    s.f = true;
                    break;
                case 4:
                    s.e = true;
                    s.f = true;
                    s.a = true;
                    break;
                case 5:
                    s.f = true;
                    s.a = true;
                    s.b = true;
                    break;
                case 6:
                    s.b = true;
                    s.a = true;
                    s.c = true;
                    break;
                case 0:
                    if (x.position == 169) {
                        s.a = true;
                        s.b = true;
                        s.c = true;
                        break;
                    }
                    if (x.position == 177) {
                        s.d = true;
                        s.b = true;
                        s.c = true;
                        break;
                    }
                    if (x.position == 185) {
                        s.d = true;
                        s.e = true;
                        s.c = true;
                        break;
                    }
                    if (x.position == 193) {
                        s.d = true;
                        s.e = true;
                        s.f = true;
                        break;
                    }
                    if (x.position == 201) {
                        s.a = true;
                        s.e = true;
                        s.f = true;
                        break;
                    }
                    if (x.position == 209) {
                        s.a = true;
                        s.f = true;
                        s.b = true;
                        break;
                    }
            }
        }
        if (x instanceof BoardBound) {
            switch (BoardBound.boardNo(((BoardBound) x).position)) {
                case 1:
                    s.b = true;
                    s.c = true;
                    break;
                case 2:
                    s.d = true;
                    s.c = true;
                    break;
                case 3:
                    s.d = true;
                    s.e = true;
                    break;
                case 4:
                    s.e = true;
                    s.f = true;
                    break;
                case 5:
                    s.f = true;
                    s.a = true;
                    break;
                case 6:
                    s.b = true;
                    s.a = true;
                    break;
                case 0:
                    if (((BoardBound) x).position == 169) {
                        s.a = true;
                        s.b = true;
                        s.c = true;
                        break;
                    }
                    if (((BoardBound) x).position == 177) {
                        s.d = true;
                        s.b = true;
                        s.c = true;
                        break;
                    }
                    if (((BoardBound) x).position == 185) {
                        s.d = true;
                        s.e = true;
                        s.c = true;
                        break;
                    }
                    if (((BoardBound) x).position == 193) {
                        s.d = true;
                        s.e = true;
                        s.f = true;
                        break;
                    }
                    if (((BoardBound) x).position == 201) {
                        s.a = true;
                        s.e = true;
                        s.f = true;
                        break;
                    }
                    if (((BoardBound) x).position == 209) {
                        s.a = true;
                        s.f = true;
                        s.b = true;
                        break;
                    }
            }
        }
        else {
            
            return s;
        }
        return s;
    }
    public String toString(){
        return "("+a+","+b+","+c+","+d+","+e+","+f+")";
    }

    public static ArrayList<Double> anglefilter(Block x){
        ArrayList<Double> angles = new ArrayList<>();
        if((x.a)){
            angles.add(180.0);
        }
        if((x.b)){
            angles.add(240.0);
        }
        if((x.c)){
            angles.add(300.0);
        }
        if((x.d)){
            angles.add(0.0);
        }
        if((x.e)){
            angles.add(60.0);
        }
        if((x.f)){
            angles.add(120.0);
        }
        return angles;
    }

    public static ArrayList<Double> boundedangle(Block x){
        ArrayList<Double> angles = new ArrayList<>();
        if((x.a)){
            angles.add(0.0);
        }
        if((x.b)){
            angles.add(60.0);
        }
        if((x.c)){
            angles.add(120.0);
        }
        if((x.d)){
            angles.add(180.0);
        }
        if((x.e)){
            angles.add(240.0);
        }
        if((x.f)){
            angles.add(300.0);
        }
        return angles;
    }


    public static List<Integer> getBoundryList() {
        return boundryList;
    }//edited by Nance
}
