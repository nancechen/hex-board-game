package comp1110.ass2;


import java.util.*;


/**
 * Created by steveb on 30/07/2015.
 */

public class HexGame {

    public String game;
    public static String nook;
    public static String cranny;
    public static String startPoints;


    /**
     * Construct HexGame from a string describing game state.
     *
     * @param game The initial state of the game, described as a string according to the assignment specification.
     */
    public HexGame(String game) {
        this.game = game;


    }

    /**
     * Construct HexGame with a random game state that complies with the assignment specification.
     */
    public HexGame() {
        cranny = Cranny.createCrannies();
        game = Game.gameConstructor(cranny);
        nook = game.substring(18, 90);
        startPoints = game.substring(90, game.length());
    }

    public HexGame(int numberPlayer) {
        cranny = Cranny.createCrannies();
        game = Game.gameConstructor(cranny);
        nook = game.substring(18, 90);
        startPoints = game.substring(90, game.length());
    }//for creating game which has multiple pieces

    public static String getNook() {
        return nook;
    }




    /**
     * Determine whether a set of crannies are legal according to the assignment specification.
     *
     * @param crannies A string describing the crannies, encoded according to the assignment specification.
     * @return True if the crannies are correctly encoded and in legal positions, according to the assignment specification.
     */
    public static boolean legitimateCrannies(String crannies) {
        if (crannies.length() != 18) {
            return false;
        }
        int C1 = Integer.parseInt(crannies.substring(0, 3));
        int C2 = Integer.parseInt(crannies.substring(3, 6));
        int C3 = Integer.parseInt(crannies.substring(6, 9));
        int C4 = Integer.parseInt(crannies.substring(9, 12));
        int C5 = Integer.parseInt(crannies.substring(12, 15));
        int C6 = Integer.parseInt(crannies.substring(15, 18));

        Boolean[] statement = {false, false, false, false, false, false};
        int check = 0;
        if (C1 >= 169 && C1 <= 176 && !statement[0]) {
            statement[0] = true;
            check++;
        }
        if (C2 >= 177 && C2 <= 184 && !statement[1]) {
            statement[1] = true;
            check++;
        }
        if (C3 >= 185 && C3 <= 192 && !statement[2]) {
            statement[2] = true;
            check++;
        }
        if (C4 >= 193 && C4 <= 200 && !statement[3]) {
            statement[3] = true;
            check++;
        }
        if (C5 >= 201 && C5 <= 208 && !statement[4]) {
            statement[4] = true;
            check++;
        }
        if (C6 >= 209 && C6 <= 216 && !statement[5]) {
            statement[5] = true;
            check++;
        }
        return (check == 6);
    }//edited by Iris

    /**
     * Determine whether a set of nooks are legal according to the assignment specification.
     *
     * @param nooks A string describing the nooks, encoded according to the assignment specification.
     * @return True if the nooks are correctly encoded and in legal positions, according to the assignment specification.
     */
    public static boolean legitimateNooks(String nooks) {
        List<String> a = new ArrayList<>();
        for (String c : nooks.split("[0-9]")) {//http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
            a.add(c);
        }
        String[] alpha_a = {"G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        for (int i = 0; i < alpha_a.length; i++) {
            if (a.contains(alpha_a[i])) {
                return false;
            }
        }
        List<Integer> n = new ArrayList<>();
        for (String b : nooks.split("[A-Z]")) {//http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
            n.add(Integer.parseInt(b));
        }
        if (n.size() != 18) {
            return false;
        }
        //create triangles
        int[] t1 = {8, 20, 21, 38, 39, 40, 62, 63, 64, 65, 92, 93, 94, 95, 96};
        int[] t2 = {10, 23, 24, 42, 43, 44, 67, 68, 69, 70, 98, 99, 100, 101, 102};
        int[] t3 = {12, 26, 27, 46, 47, 48, 72, 73, 74, 75, 104, 105, 106, 107, 108};
        int[] t4 = {14, 29, 30, 50, 51, 52, 77, 78, 79, 80, 110, 111, 112, 113, 114};
        int[] t5 = {16, 32, 33, 54, 55, 56, 82, 83, 84, 85, 116, 117, 118, 119, 120};
        int[] t6 = {18, 35, 36, 58, 59, 60, 87, 88, 89, 90, 122, 123, 124, 125, 126};

        List<Integer> aList = new ArrayList<>();
        List<Integer> bList = new ArrayList<>();
        List<Integer> cList = new ArrayList<>();
        List<Integer> dList = new ArrayList<>();
        List<Integer> eList = new ArrayList<>();
        List<Integer> fList = new ArrayList<>();
        for (int x : n) {
            for (int i = 0; i < t1.length; i++) {
                if (t1[i] == x) {
                    aList.add(x);
                    break;
                }
            }
            for (int i = 0; i < t1.length; i++) {
                if (t2[i] == x) {
                    bList.add(x);
                    break;
                }
            }
            for (int i = 0; i < t1.length; i++) {
                if (t3[i] == x) {
                    cList.add(x);
                    break;
                }
            }
            for (int i = 0; i < t1.length; i++) {
                if (t4[i] == x) {
                    dList.add(x);
                    break;
                }
            }
            for (int i = 0; i < t1.length; i++) {
                if (t5[i] == x) {
                    eList.add(x);
                    break;
                }
            }
            for (int i = 0; i < t1.length; i++) {
                if (t6[i] == x) {
                    fList.add(x);
                    break;
                }
            }
        }
        int ok = 0;
        if (aList.size() == 3 && bList.size() == 3 && cList.size() == 3 && dList.size() == 3 && eList.size() == 3 && fList.size() == 3) {
            List<Integer> t1P = Nook.Nookmethods.get_position(aList);
            List<Integer> t2P = Nook.Nookmethods.get_position(bList);
            List<Integer> t3P = Nook.Nookmethods.get_position(cList);
            List<Integer> t4P = Nook.Nookmethods.get_position(dList);
            List<Integer> t5P = Nook.Nookmethods.get_position(eList);
            List<Integer> t6P = Nook.Nookmethods.get_position(fList);
            if ((Nook.Nookmethods.method_a(aList, t1P)) && (Nook.Nookmethods.method_b(bList, t2P)) && (Nook.Nookmethods.method_c(cList, t3P)) && (Nook.Nookmethods.method_d(dList, t4P)) && (Nook.Nookmethods.method_e(eList, t5P)) && Nook.Nookmethods.method_f(fList, t6P)) {
                return true;
            }
        }
        return false;
    }//edited by Lucy ,Iris and Ke Chen

    /**
     * Determine whether a game state is legal according to the assignment specification.
     *
     * @param game A string describing the game state, encoded according to the assignment specification.
     * @return True if the game state is correctly encoded and represents a legal game state, according to the assignment specification.
     */

    public static boolean legitimateGame(String game) {
        int length = game.length();
        String Crannies = game.substring(0, 18);
        String Nooks = game.substring(18, 90);
        String startpositions = game.substring(90, length);
        List<Integer> StP = new ArrayList<Integer>();
        for (int i = 0; i < length - 90; i = i + 3) {
            StP.add(Integer.parseInt(startpositions.substring(i, i + 3)));
        }

        List<Integer> nook = Game.getNook(game);//without direction

        Boolean legit;
        Boolean contain;
        Boolean dulipcate;



        if (legitimateCrannies(Crannies) && legitimateNooks(Nooks)) {
            legit = true;
        }else {
            legit = false;
        }


        if (nook.containsAll(StP)) {
            contain = true;
        }else {
            contain = false;
        }

        Set<Integer> nodulipcate = new HashSet<>();
        nodulipcate.addAll(StP);
        if (nodulipcate.size() == StP.size()){
            dulipcate = true;
        }else {
            dulipcate = false;
        }
        return legit && contain && dulipcate;
    }//edited by Iris and Lucy



    /**
     * Determine whether a given step is legal according to a given game state and the assignment specification.
     *
     * @param game A string describing the game state, encoded according to the assignment specification.
     * @param from The point from which the step starts
     * @param to   The point to which step goes
     * @return True if the move is legal according to the assignment specification.
     */

    // check whether the step is legistimate edited by Ke Chen
    public static boolean legitimateStep(String game, int from, int to) {
        int length = game.length();
        String Crannies = game.substring(0, 18);
        String Nooks = game.substring(18, 90);
        String startpositions = game.substring(90, length);
        List<Integer> StP = new ArrayList<>();
        for (int i = 0; i < length - 90; i = i + 3) {
            Pieces.addPiecePosition(Integer.parseInt(startpositions.substring(i, i + 3)));
        }//get a list of start points

        List<Integer> nook = Game.getNook(game);//without direction

        List<Integer> cranny = Game.getCranny(game);

        List<Integer> boundry = Block.getBoundryList();


        Point toP = Point.convertHex(to);
        Point fromP = Point.convertHex(from);

        boolean b = Pieces.getPiecePositions().contains(from);//check start point
        boolean c = nook.contains(to);//check to
        boolean d = cranny.contains(to);//check to
        boolean e = boundry.contains(to);//check to
        boolean oneStep = Point.validAngle(fromP, toP);

        boolean noNook = checkNoNook(from, to, nook);

        boolean noCranny = checkNoCranny(to, from, cranny);


        boolean BarrierBlock = checkBarrierBlock(from, to, nook, cranny, Nooks);//if it is true, it is not accessible
        boolean barrierMakeStop = neighbourMakeStop(from, to, nook, cranny, Nooks);

        if (b) {
            if (oneStep && noCranny && noNook) {
                if (!BarrierBlock) {
                    //piece to nook,cranny,boudry
                    if (c || d) {
                        return true;
                    } else if (e && !checkBoundry(from, to, nook, cranny, Nooks) && boardStop(from, to, cranny)) {
                        return true;
                    } else if (barrierMakeStop) {
                        return true;
                    }
                }
            }
        } else
            return false;

        return false;


    } 

    //check whether there is nook in the way,edited by Ke Chen
    public static boolean checkNoNook(int from, int to, List<Integer> nook) {

        for (int i = 0; i < 18; i++) {
            if (nook.get(i) != to && nook.get(i) != from && checkInLine(from, to, nook.get(i))) {
                return false;
            }
        }

        return true;
    }

    //check whether there is cranny in the way,edited by Ke Chen
    public static boolean checkNoCranny(int to, int from, List<Integer> cranny) {
        Point toP = Point.convertHex(to);
        Point fromP = Point.convertHex(from);
        List<Integer> boundry = Block.getBoundryList();

        for (int i = 0; i < 6; i++) {
            Point crannyP = Point.convertHex(cranny.get(i));
            int craa = cranny.get(i);
            boolean b = Math.abs(Point.getAngle(fromP, crannyP) - Point.getAngle(fromP, toP)) <= 1;
            if (craa != to && craa != from && checkInLine(from, to, craa)) {
                return false;

            }
        }
        return true;
    }

    //check whether the move along the board can stop,edited by Ke Chen
    public static boolean boardStop(int from, int to, List<Integer> cranny) {
        if (BoardBound.checkSameSide(to, from)) {
            if (to > from && cranny.contains(to)) {
                return true;
            } else if (to < from && cranny.contains(to - 1)) {
                return true;
            } else if (!cranny.contains(to) ) {
                if (to == 177 || to == 185 || to == 193 || to == 201 || to == 209 || to == 216 || to == 169)
                    return true;
            }

        } else if (from == 169 && BoardBound.boardNo(to).equals(6) && cranny.contains(to - 1)) {
            return true;
        } else if (!SetNook.isAngleBounded(new BoardBound(from, Point.convertHex(from)),
                new BoardBound(to, Point.convertHex(to)))) {
            return true;
        }
        return false;
    }

    public static boolean checkBoundry(int from, int to, List<Integer> nook, List<Integer> cranny, String Nooks) {
        List<Integer> boundry = Block.getBoundryList();
        if (nook.contains(from)) {
            return SetNook.isAngleBounded(new Nook(from, Nook.getOrientation(from, Nooks), Point.convertHex(from)),
                    new BoardBound(to, Point.convertHex(to)));
        } else if (cranny.contains(from)) {
            return SetNook.isAngleBounded(new Cranny(from, Point.convertHex(from)),
                    new BoardBound(to, Point.convertHex(to)));
        } else if (boundry.contains(from)) {

            return false;

        } else
            return SetNook.isAngleBounded(new Hexgon(from, Point.convertHex(from)),
                    new BoardBound(to, Point.convertHex(to)));

    }//edited by Ke Chen
    //to its boundry,false is accessible ,true is not accessible


//check whether there is boundry next to the to to make the move stop,edited by Ke Chen
    public static boolean neighbourMakeStop(int from, int to, List<Integer> nook, List<Integer> cranny, String Nooks) {

        List<Integer> boundry = Block.getBoundryList();

        for (int i = 0; i < 18; i++) {
            Point nookP = Point.convertHex(nook.get(i));
            Nook nookH = new Nook(nook.get(i), Nook.getOrientation(nook.get(i), Nooks), nookP);

            if (checkInLine(from, nook.get(i), to) && Math.abs(SetNook.getDistance(nook.get(i), to) - 20.0 * Math.sqrt(3)) <= 1) {
                if (nook.contains(from)) {
                    Nook fromH = new Nook(from, Nook.getOrientation(from, Nooks), Point.convertHex(from));
                    return SetNook.isAngleBounded(nookH, fromH);
                } else if (cranny.contains(from)) {
                    Cranny fromC = new Cranny(from, Point.convertHex(from));
                    return SetNook.isAngleBounded(nookH, fromC);
                } else if (boundry.contains(from)) {
                    BoardBound fromB = new BoardBound(from, Point.convertHex(from));
                    return SetNook.isAngleBounded(nookH, fromB);
                } else {
                    return SetNook.isAngleBounded(nookH, new Hexgon(from, Point.convertHex(from)));

                }
            }
        }
        return false;
    }

    //check whether there is barrier around makes the move stop, edited by Ke Chen
    public static boolean checkBarrierBlock(int from, int to, List<Integer> nook, List<Integer> cranny, String Nooks) {

        List<Integer> boundry = Block.getBoundryList();
        if (nook.contains(to) && nook.contains(from)) {
            return SetNook.isAngleBounded(new Nook(to, Nook.getOrientation(to, Nooks), Point.convertHex(to)),
                    new Nook(from, Nook.getOrientation(from, Nooks), Point.convertHex(from)));

        } else if (cranny.contains(to) && nook.contains(from)) {
            return SetNook.isAngleBounded(new Cranny(to, Point.convertHex(to)),
                    new Nook(from, Nook.getOrientation(from, Nooks), Point.convertHex(from)));
        } else if (cranny.contains(from) && nook.contains(to)) {
            return SetNook.isAngleBounded(new Nook(to, Nook.getOrientation(to, Nooks), Point.convertHex(to)),
                    new Cranny(from, Point.convertHex(from)));

        } else if (nook.contains(to) && boundry.contains(from)) {
            return SetNook.isAngleBounded(new Nook(to, Nook.getOrientation(to, Nooks), Point.convertHex(to)),
                    new BoardBound(from, Point.convertHex(from)));
        } else if (nook.contains(from) && boundry.contains(to)) {
            return SetNook.isAngleBounded(new BoardBound(to, Point.convertHex(to)),
                    new Nook(from, Nook.getOrientation(from, Nooks), Point.convertHex(from)));
        } else if (cranny.contains(to) && cranny.contains(from)) {
            return SetNook.isAngleBounded(new Cranny(to, Point.convertHex(to)),
                    new Cranny(from, Point.convertHex(from)));
        } else if (cranny.contains(to) && boundry.contains(from)) {
            return SetNook.isAngleBounded(new Cranny(to, Point.convertHex(to)),
                    new BoardBound(from, Point.convertHex(from)));
        } else if (boundry.contains(to) && cranny.contains(from)) {
            return SetNook.isAngleBounded(new Cranny(from, Point.convertHex(from)),
                    new BoardBound(to, Point.convertHex(to)));
        } else if (nook.contains(to) && !nook.contains(from) && !cranny.contains(from) && !boundry.contains(from)) {
            return SetNook.isAngleBounded(new Nook(to, Nook.getOrientation(to, Nooks), Point.convertHex(to)),
                    new Hexgon(from, Point.convertHex(from)));
        } else if (nook.contains(from) && !nook.contains(to) && !cranny.contains(to) && !boundry.contains(to)) {
            return SetNook.isAngleBounded(new Nook(from, Nook.getOrientation(from, Nooks), Point.convertHex(from)),
                    new Hexgon(to, Point.convertHex(to)));
        } else if (cranny.contains(from) && !nook.contains(to) && !cranny.contains(to) && !boundry.contains(to)) {
            return SetNook.isAngleBounded(new Cranny(from, Point.convertHex(from)),
                    new Hexgon(to, Point.convertHex(to)));
        } else if (cranny.contains(to) && !nook.contains(from) && !cranny.contains(from) && !boundry.contains(from)) {
            return SetNook.isAngleBounded(new Cranny(to, Point.convertHex(to)),
                    new Hexgon(from, Point.convertHex(from)));
        } else return false;
    }
//check whether three integers are in the same line, edited by Ke Chen
    public static boolean checkInLine(int from, int to, int middle) {
        Point toP = Point.convertHex(to);
        Point fromP = Point.convertHex(from);
        Point middleP = Point.convertHex(middle);
        boolean xmax = Math.abs(middleP.getX() + 350) <= Math.max(Math.abs(toP.getX() + 350), Math.abs(fromP.getX() + 350));
        boolean xmin = Math.abs(middleP.getX() + 350) >= Math.min(Math.abs(toP.getX() + 350), Math.abs(fromP.getX() + 350));
        boolean ymax = Math.abs(middleP.getY() + 350) <= Math.max(Math.abs(toP.getY() + 350), Math.abs(fromP.getY() + 350));
        boolean ymin = Math.abs(middleP.getY() + 350) >= Math.min(Math.abs(toP.getY() + 350), Math.abs(fromP.getY() + 350));
        boolean angle = Math.abs(Point.getAngle(fromP, middleP) - (Point.getAngle(fromP, toP))) <= 1;

        if (xmax && xmin && ymax && ymin) {
            if (angle) {
                return true;
            }
        }
        return false;
    }

    public static boolean legalStep(String game, int from, int to) {
        int length = game.length();
        String Crannies = game.substring(0, 18);
        String Nooks = game.substring(18, 90);
        String startpositions = game.substring(90, length);
        List<Integer> StP = new ArrayList<>();
        for (int i = 0; i < length - 90; i = i + 3) {
            Pieces.addPiecePosition(Integer.parseInt(startpositions.substring(i, i + 3)));
        }//get a list of start points

        List<Integer> nook = Game.getNook(game);//without direction
        List<Integer> cranny = Game.getCranny(game);
        List<Integer> boundry = Block.getBoundryList();

        Point toP = Point.convertHex(to);
        Point fromP = Point.convertHex(from);

        boolean b = true;//check start point
        boolean c = nook.contains(to);//check to
        boolean d = cranny.contains(to);//check to
        boolean e = boundry.contains(to);//check to
        boolean oneStep = Point.validAngle(fromP, toP);

        boolean noNook = checkNoNook(from, to, nook);

        boolean noCranny = checkNoCranny(to, from, cranny);


        boolean BarrierBlock = checkBarrierBlock(from, to, nook, cranny, Nooks);//if it is true, it is not accessible
        boolean barrierMakeStop = neighbourMakeStop(from, to, nook, cranny, Nooks);

        if (b) {
            if (oneStep && noCranny && noNook) {
                if (!BarrierBlock) {
                    //piece to nook,cranny,boudry
                    if (c || d) {
                        return true;
                    } else if (e && !checkBoundry(from, to, nook, cranny, Nooks) && boardStop(from, to, cranny)) {
                        return true;
                    } else if (barrierMakeStop) {
                        return true;
                    }
                }
            }
        } else
            return false;

        return false;
    }


    /* }


     /**
      * Return a minimal path from start to goal.
      *
      * @param game  A string describing the game state, encoded according to the assignment specification.
      * @param start The point from which the path must start
      * @param goal  The point at which the path must end
      * @return An array of integers reflecting a minimal path from start to goal, each integer reflecting a node on the board, starting with the start, and ending with the goal.
      */

     //edited by Ke Chen, find minial path by using recursion and legitimateStep
    public static int[] minimalPath(String game, int start, int goal) {
        int[] solution = null;
        int i = 1;
        while (i < 13 && solution == null) {
            List<Integer> temp = recursiveMinimalPath(game, start, goal, i);
            if (temp != null && temp.size() > 0) {
                temp.add(0, start);
                solution = listToArray(temp);
            }
            i++;
        }
        return solution;
    }

    private static int[] listToArray(List<Integer> list) {
        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++)
            result[i] = list.get(i);
        return result;
    }

    private static List<Integer> recursiveMinimalPath(String game, int start, int goal, int counter) {
        List<Integer> steps = possibleSteps(game, start);
        if (steps.contains(goal)) {
            List<Integer> result = new ArrayList<>();
            result.add(goal);
            return result;
        } else if (counter == 0) {
            return null;
        } else {
            for (int step : steps) {
                List<Integer> temp = recursiveMinimalPath(game, step, goal, counter - 1);
                if (temp != null) {
                    temp.add(0, step);
                    return temp;
                }
            }
        }
        return null;
    }

    public static List<Integer> possibleSteps(String game, int from) {
        List<Integer> steps = new ArrayList<>();
        for (int i = 0; i <= 216; i++) {
            if (legalStep(game, from, i))
                steps.add(i);
        }
        return steps;
    }
    //find all the possilble steps in the board


    /**
     * Output the state of the game as a string, encoded according to the assignment specification
     *
     * @return A string that reflects the game state, encoded according to the assignment specification.
     */

    public String toString() {

        return game;
    }


}

