package comp1110.ass2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Nance on 30/08/2015.
 */
public class Cranny extends Hexgon {
    private String crannies;


    public Cranny(int p, Point coordinate){
        super(p,coordinate);
        crannies = p + "";
    }


    public static String createCrannies() {
        Random random = new Random();
        String crannies = "";
        int a = random.nextInt(8) + 169;
        int b = random.nextInt(8) + 177;
        int c = random.nextInt(8) + 185;
        int d = random.nextInt(8) + 193;
        int e = random.nextInt(8) + 201;
        int f = random.nextInt(8) + 209;

        crannies = crannies + Integer.toString(a)+Integer.toString(b)+Integer.toString(c)+Integer.toString(d)+Integer.toString(e)
        +Integer.toString(f);

        return crannies;
    }


    public String toString() {
        return crannies;
    }


    public static ArrayList<Hexgon> getCrannies(String crannies){
        ArrayList<Hexgon> crannylist = new ArrayList<>();
        if(crannies.length()!=18){
            return crannylist;
        }
        int C1 = Integer.parseInt(crannies.substring(0, 3));
        crannylist.add(new Cranny(C1,Point.convertHex(C1)));
        int C2 = Integer.parseInt(crannies.substring(3, 6));
        crannylist.add(new Cranny(C2,Point.convertHex(C2)));
        int C3 = Integer.parseInt(crannies.substring(6, 9));
        crannylist.add(new Cranny(C3,Point.convertHex(C3)));
        int C4 = Integer.parseInt(crannies.substring(9, 12));
        crannylist.add(new Cranny(C4,Point.convertHex(C4)));
        int C5 = Integer.parseInt(crannies.substring(12,15));
        crannylist.add(new Cranny(C5,Point.convertHex(C5)));
        int C6 = Integer.parseInt(crannies.substring(15,18));
        crannylist.add(new Cranny(C6,Point.convertHex(C6)));
        return crannylist;
    }
    public static ArrayList<Integer> getCranniesAndNeighbourPositions(String crannies){
        ArrayList<Integer> crannylist = new ArrayList<>();
        if(crannies.length()!=18){
            return crannylist;
        }
        int C1 = Integer.parseInt(crannies.substring(0, 3));
        crannylist.add(C1);
        crannylist.add(C1+1);
        int C2 = Integer.parseInt(crannies.substring(3, 6));
        crannylist.add(C2);
        crannylist.add(C2+1);
        int C3 = Integer.parseInt(crannies.substring(6, 9));
        crannylist.add(C3);
        crannylist.add(C3+1);
        int C4 = Integer.parseInt(crannies.substring(9, 12));
        crannylist.add(C4);
        crannylist.add(C4+1);
        int C5 = Integer.parseInt(crannies.substring(12,15));
        crannylist.add(C5);
        crannylist.add(C5+1);
        int C6 = Integer.parseInt(crannies.substring(15,18));
        crannylist.add(C6);
        crannylist.add(C6+1);
        return crannylist;
    }

    public static ArrayList CCoords(ArrayList<Cranny> crannies){
        ArrayList<Point> s = new ArrayList<>();
        for(Cranny x:crannies){
            s.add(Point.convertHex(x.position));
        }
        return s;
    }

}