package comp1110.ass2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sining on 2015/10/4.
 * A class that takes a String and find Barriers of this game. It's created for saving the time repeating this step that takes out crannies and nooks.
 */
public class Game {
    private final List<Hexgon> nooks;
    private final List<Hexgon> crannies;
    private final List<Hexgon> boardBounds;

    public static String gameConstructor(String crannies){
        String nooks = Nook.createNooksIris(crannies);
        String piece = Pieces.createPieces(nooks);
        String gameString = crannies+nooks+piece;
        if(gameString.length()<93){
            gameString = gameConstructor(crannies);
        }
        if(HexGame.legitimateGame(gameString)){
            return gameString;
        }else {
            gameString=gameConstructor(crannies);}
        return gameString;
    }

    public Game(final String gameString) {
        int length = gameString.length();
        String Crannies = gameString.substring(0, 18);
        String Nooks = gameString.substring(18, 90);
        String startpositions = gameString.substring(90, length);

        nooks = Hexgon.combine(Nook.getNooks(Nooks));
        crannies = Hexgon.combine(Cranny.getCrannies(Crannies));
        boardBounds = BoardBound.getBoardBounds(Crannies);


    }

    public static List<Integer> getCranny(String gameString) {

        String Crannies = gameString.substring(0, 18);
        List<Integer> cranny = new ArrayList<>();
        for (int i = 0; i < 18; i = i + 3) {
            cranny.add(Integer.parseInt(Crannies.substring(i, i + 3)));
        }
        return cranny;
    }

    public static List<Integer> getNook(String gameString){
        List<Integer> nook = new ArrayList<>();
        String Nooks = gameString.substring(18, 90);

        for (String b : Nooks.split("[A-Z]")) {
            nook.add(Integer.parseInt(b));
        }
        return nook;
    }

    public List<Hexgon> getNooks() {
        return nooks;
    }

    public List<Hexgon> getCrannies() {
        return crannies;
    }

    public List<Hexgon> getBoardBounds() {
        return boardBounds;
    }
}
