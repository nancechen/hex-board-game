package comp1110.ass2;

/**
 * Created by Nance on 25/09/2015.
 */
public class Point {
    public static final double SIDE = 20.0;
    public static final double HEIGHT = SIDE / 2 * Math.sqrt(3); //Triangle height (shorter diameter / 2)

    public final static Point[] hexPoints = new Point[217];

    static {
        hexPoints[0] = new Point(0, 0);
        double x = -HEIGHT;
        double y = 3 * SIDE / 2;
        int index = 1;

        for (int i = 1; i <= 8; i++) {
            y -= SIDE * 3;
            for (int j = 0; j < i * 6; j++) {
                switch ((j + i - 1) / i) {
                    case 1:
                        x += HEIGHT * 2;
                        break;
                    case 2:
                        x += HEIGHT;
                        y += SIDE * 3 / 2;
                        break;
                    case 3:
                        x -= HEIGHT;
                        y += SIDE * 3 / 2;
                        break;
                    case 4:
                        x -= HEIGHT * 2;
                        break;
                    case 5:
                        x -= HEIGHT;
                        y -= SIDE * 3 / 2;
                        break;
                    case 6:
                        x += HEIGHT;
                        y -= SIDE * 3 / 2;
                        break;
                    default:
                }
                hexPoints[index++] = new Point(x, y);
            }
        }
    } //end of initializing,

    private final double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Point convertHex(int n) {
        return hexPoints[n];
    }//convert all integers to Point in XY coordinate system

    public static int convertPoint(Point p){
        for (int i=0;i<217;i++){
            Point hP = hexPoints[i];
            if (hP.getX() ==p.getX()&& hP.getY()==p.getY()){
                return  i;
            }
        }
        return -1;
    }//convert a point to integer it represents

    public static double angle(Point p1, Point p2) {
        if (p1.getX() == p2.getX())
            return 90;

        double slope = (p1.getY() - p2.getY()) / (p1.getX() - p2.getX());
        return 180 / Math.PI * Math.atan(slope);

    }//return angle from -90 to 90
    public static boolean checkSide(Point p1,Point p2){
        if (p1.getY()==p2.getY()&&p1.getX()<p2.getX())
            return true;
        if (p1.getY()==p2.getY()&&p1.getX()>p2.getX())
            return false;
        return false;
    }//check whether two points are in the same side

    public static boolean validAngle(Point p1, Point p2) {
        if ((Math.round(angle(p1, p2))) % 60 == 0)
            return true;
        return false;
    }

    public static double getAngle(Point p1,Point p2) {

        double angle = Math.toDegrees(Math.atan2(p1.getY() - p2.getY(), p1.getX() - p2.getX()));
        if(angle < 0){
            angle += 360;
        }
        return angle;
    }//return angle from 0 to 360.edited by Iris

    public String toString(){
        return "("+x+","+y+")";
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
