package comp1110.ass2;/**
 * Created by Nance on 23/09/2015.
 */

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class HexBoard extends Application {
    public final static int SIZE = 20;
    public static final int HEIGHT = 700;
    public static final int WIDTH = 700;
    public static final int CENTER_X = WIDTH / 2;
    public static final int CENTER_Y = HEIGHT / 2;
    private Group gameLayer;
    private Group root;
    private Scene scene;
    private Group mainMenu;
    private Group lineLayer;
    private int numberPlayer = 1;
    private int numberPiece = 1;
    private HexGame hg;
    private int player;
    private int goal;
    private String game;
    private String Nooks;
    private List<Integer> nook;
    private List<Integer> cranny;
    private ArrayList<Integer> solutionList;
    private Text warning;
    private Text solutionResult;
    private Text stepResult;
    private Text congrat;
    private List<Integer> steps = new ArrayList<>();
    private int[][] boxPosition = new int[][]{
            {20, 20},
            {20, HEIGHT - 70},
            {WIDTH - 250, 20},
            {WIDTH - 250, HEIGHT - 70}
    };
    private List<Integer> inputs = new ArrayList<>();
    private List<Integer> playerNo = new ArrayList<>();
    private Integer min;
    private int index;

    //private int[][] inputs;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("HexGame");
        gameLayer = new Group();
        root = new Group();
        mainMenu = new Group();
        lineLayer = new Group();
        scene = new Scene(root, WIDTH, HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();

        //Main menu
        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 100));
        scenetitle.setLayoutX(scene.getWidth() / 4);
        scenetitle.setLayoutY(scene.getHeight() / 2);
        mainMenu.getChildren().add(scenetitle);

        root.getChildren().add(mainMenu);


        //one player setting
        Button btn1 = new Button("One player");
        HBox hbBtn = new HBox(400);
        hbBtn.getChildren().add(btn1);
        hbBtn.setLayoutX(scene.getWidth() / 4);
        hbBtn.setLayoutY(CENTER_Y + 50);
        mainMenu.getChildren().add(hbBtn);


        btn1.setOnMouseClicked(event -> {
            numberPlayer = 1;
            root.getChildren().remove(mainMenu);
            root.getChildren().add(gameLayer);
            root.getChildren().add(lineLayer);
            gameInitialize(numberPiece);
            addSingleButton();



        });

        //two players setting

        Button btn2 = new Button("Two players");
        HBox hbBtn2 = new HBox(400);
        hbBtn2.getChildren().add(btn2);
        hbBtn2.setLayoutX(scene.getWidth() / 4);
        hbBtn2.setLayoutY(CENTER_Y + 100);
        mainMenu.getChildren().add(hbBtn2);


        btn2.setOnMouseClicked(event -> {
            numberPlayer = 2;
            root.getChildren().remove(mainMenu);
            root.getChildren().add(gameLayer);
            root.getChildren().add(lineLayer);
            addInputBox(numberPlayer);
            gameInitialize(numberPiece);
            addTimesOutButton();

        });

        //three players setting

        Button btn3 = new Button("Three players");
        HBox hbBtn3 = new HBox(400);
        hbBtn3.getChildren().add(btn3);
        hbBtn3.setLayoutX(scene.getWidth() / 4);
        hbBtn3.setLayoutY(CENTER_Y + 150);
        mainMenu.getChildren().add(hbBtn3);


        btn3.setOnMouseClicked(event -> {
            numberPlayer = 3;
            root.getChildren().remove(mainMenu);
            root.getChildren().add(gameLayer);
            root.getChildren().add(lineLayer);
            addInputBox(numberPlayer);
            gameInitialize(numberPiece);
            addTimesOutButton();

        });

        //four player setting
        Button btn4 = new Button("Four players");
        HBox hbBtn4 = new HBox(400);
        hbBtn4.getChildren().add(btn4);
        hbBtn4.setLayoutX(scene.getWidth() / 4);
        hbBtn4.setLayoutY(CENTER_Y + 200);
        mainMenu.getChildren().add(hbBtn4);


        btn4.setOnMouseClicked(event -> {
            numberPlayer = 4;
            root.getChildren().remove(mainMenu);
            root.getChildren().add(gameLayer);
            root.getChildren().add(lineLayer);
            addInputBox(numberPlayer);
            gameInitialize(numberPiece);
            addTimesOutButton();


        });

        Button rule = new Button("rules");
        HBox ru = new HBox(400);
        ru.getChildren().add(rule);
        ru.setLayoutX(scene.getWidth() / 4);
        ru.setLayoutY(CENTER_Y + 250);
        mainMenu.getChildren().add(ru);

        rule.setOnMouseClicked(event -> {
            root.getChildren().remove(mainMenu);
            Image img = new Image("gamerule.PNG", 500, 600, true, true);
            ImageView iv = new ImageView(img);
            iv.setLayoutX(0);
            iv.setLayoutY(0);
            root.getChildren().add(iv);
            addBackButtom();
        });



        //drawing hexagon and outerboundry
        drawBoard();
        removeAllButton();
        setTimer();





    }

    public void setTimer(){
        Label timerLabel = new Label();
        Integer STARTTIME = 60;
        IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);
        // Bind the timerLabel text property to the timeSeconds property
        timerLabel.textProperty().bind(timeSeconds.asString());
        timerLabel.setTextFill(Color.RED);
        timerLabel.setStyle("-fx-font-size: 1.4em;");




        Button button = new Button();
        button.setText("Start Timer");
        button.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {
                Timeline timeline=new Timeline();
                if (timeline != null) {
                    timeline.stop();
                }
                timeSeconds.set(STARTTIME);
                timeline = new Timeline();
                timeline.getKeyFrames().add(
                        new KeyFrame(Duration.seconds(STARTTIME + 1),
                                new KeyValue(timeSeconds, 0)));
                timeline.playFromStart();
            }
        });

        VBox vb = new VBox(10);             // gap between components is 20


        //vb.setPrefWidth(scene.getWidth());
        vb.getChildren().addAll(button, timerLabel);
        vb.setLayoutY(110);
        vb.setLayoutX(30);//see originality

        gameLayer.getChildren().add(vb);
    }
//add buttom for going back to menu
    public void addBackButtom(){
        Button back = new Button("back to Menu");
        HBox r = new HBox(400);
        r.getChildren().add(back);
        r.setLayoutX(30);
        r.setLayoutY(500);
        root.getChildren().add(r);

        back.setOnMouseClicked(event -> {
            root.getChildren().clear();
            root.getChildren().add(mainMenu);

        });

    }

//add input box for player to input the value of steps they come up with
    public void addInputBox(int numberPlayer) {

        for (int i = 0; i < numberPlayer; i++) {
            Label player = new Label("player" + (i + 1) + ":");

            player.setLayoutX(boxPosition[i][0]);
            player.setLayoutY(boxPosition[i][1]);
            gameLayer.getChildren().add(player);

            TextField userTextField = new TextField();
            userTextField.setLayoutX(boxPosition[i][0] + 50);
            userTextField.setLayoutY(boxPosition[i][1]);
            gameLayer.getChildren().add(userTextField);

            Button confirm = new Button("Confirm");
            HBox conb = new HBox(200);
            conb.getChildren().add(confirm);
            conb.setLayoutX(boxPosition[i][0] + 148);
            conb.setLayoutY(boxPosition[i][1] + 28);
            gameLayer.getChildren().add(conb);
            int pl = i + 1;

            confirm.setOnMouseClicked(event -> {

                String inputValue = userTextField.getCharacters().toString();
                int valueNumber = Integer.parseInt(inputValue);
                inputs.add(valueNumber);
                playerNo.add(pl);



            });


        }

    }

    public int findWinner(List<Integer> inputs, List<Integer> playerNo) {

        min = Integer.MAX_VALUE;
        int result;

        for (Integer value : inputs) {
            if (min > value) {
                min = value;

            }
        }
        index = inputs.indexOf(min);
        result = playerNo.get(index);
        return result;
    }

    public void addTimesOutButton() {


        Button times = new Button("Submit Input");
        HBox sub = new HBox(200);
        sub.getChildren().add(times);
        sub.setLayoutX(20);
        sub.setLayoutY(200);
        gameLayer.getChildren().add(sub);


        times.setOnMouseClicked(event -> {
            int Winner = findWinner(inputs, playerNo);
            congrat = new Text("Player" + Winner + " draw the step");
            congrat.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
            congrat.setFill(Color.RED);
            congrat.setLayoutX(scene.getWidth() / 4 + 70);
            congrat.setLayoutY(30);
            gameLayer.getChildren().addAll(congrat);
            addFinalButton();


        });
    }

    //after find the player who gives the best solution, let the player draw the step.
    //if solution is wrong, let next player draw the step.
    //if solution is right


    public void addFinalButton() {


        Button submit = new Button("Submit Step");
        HBox sub = new HBox(200);
        sub.getChildren().add(submit);
        sub.setLayoutX(scene.getWidth() / 4 + 110);
        sub.setLayoutY(scene.getHeight() - 60);
        gameLayer.getChildren().add(sub);


        submit.setOnMouseClicked(event -> {
            gameLayer.getChildren().remove(solutionResult);


            if (checkSolution(goal, steps.get(steps.size() - 1)) && steps.size()  == min) {

                addSolutionButton();
                gameLayer.getChildren().remove(congrat);

                solutionResult = new Text("Correct solution");
                solutionResult.setLayoutX(scene.getWidth() / 4 + 150);
                solutionResult.setLayoutY(scene.getHeight() - 15);
                gameLayer.getChildren().add(solutionResult);
                solutionResult.setFill(Color.FIREBRICK);




            } else {


                inputs.remove(inputs.get(index));
                playerNo.remove(playerNo.get(index));


                steps = new ArrayList<Integer>();
                lineLayer.getChildren().clear();

                solutionResult = new Text("Wrong solution");
                solutionResult.setLayoutX(scene.getWidth() / 4 + 150);
                solutionResult.setLayoutY(scene.getHeight() - 15);
                gameLayer.getChildren().add(solutionResult);
                solutionResult.setFill(Color.FIREBRICK);
                gameLayer.getChildren().remove(congrat);

                //rechoosing the min


                if (inputs.size() != 0) {

                    int Winner = findWinner(inputs, playerNo);

                    congrat = new Text("Player" + Winner + " draw the step");
                    congrat.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
                    congrat.setFill(Color.RED);
                    congrat.setLayoutX(scene.getWidth() / 4 + 70);
                    congrat.setLayoutY(30);
                    gameLayer.getChildren().addAll(congrat);


                }

                else {
                    solutionResult = new Text("Game Over");
                    solutionResult.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
                    solutionResult.setFill(Color.RED);
                    solutionResult.setLayoutX(scene.getWidth() / 4 + 100);
                    solutionResult.setLayoutY(70);
                    gameLayer.getChildren().add(solutionResult);
                    solutionResult.setFill(Color.FIREBRICK);




                }
            }


        });

    }

//check whether it reaches the goal
    public boolean checkSolution(int goal, int currentPosition) {
        if (goal == currentPosition) {
            return true;
        } else
            return false;
    }
    //check whether the solution is the the best solution

    public void addSolutionButton() {
        Button solution = new Button("Show best solution");
        HBox solu = new HBox(200);
        solu.getChildren().add(solution);
        solu.setLayoutX(scene.getWidth() / 4 + 100);
        solu.setLayoutY(scene.getHeight() - 60);
        gameLayer.getChildren().add(solu);


        solution.setOnMouseClicked(event -> {


            if (solutionList.size() - 1 == steps.size()) {
                gameLayer.getChildren().remove(solutionResult);

                stepResult = new Text("It is a minimal path.");
                stepResult.setLayoutX(scene.getWidth() / 4 + 100);
                stepResult.setLayoutY(scene.getHeight() - 15);
                gameLayer.getChildren().add(stepResult);
                stepResult.setFill(Color.FIREBRICK);


            } else {
                drawSolution(solutionList);
            }


        });
    }


    //submit steps button for single player
    public void addSingleButton() {


        Button submit = new Button("Submit");
        HBox sub = new HBox(200);
        sub.getChildren().add(submit);
        sub.setLayoutX(scene.getWidth() / 4 + 150);
        sub.setLayoutY(scene.getHeight() - 60);
        gameLayer.getChildren().add(sub);


        submit.setOnMouseClicked(event -> {
            gameLayer.getChildren().remove(solutionResult);
            addSolutionButton();


            if (checkSolution(goal, steps.get(steps.size() - 1))) {

                solutionResult = new Text("Correct solution");
                solutionResult.setLayoutX(scene.getWidth() / 4 + 150);
                solutionResult.setLayoutY(scene.getHeight() - 15);
                gameLayer.getChildren().add(solutionResult);
                solutionResult.setFill(Color.FIREBRICK);




            } else {
                solutionResult = new Text("Wrong solution");
                solutionResult.setLayoutX(scene.getWidth() / 4 + 150);
                solutionResult.setLayoutY(scene.getHeight() - 15);
                gameLayer.getChildren().add(solutionResult);
                solutionResult.setFill(Color.FIREBRICK);


            }


        });

    }
//redo steps for player
    public void removeAllButton() {


        Button remove = new Button("Redo");
        HBox re = new HBox(200);
        re.getChildren().add(remove);
        re.setLayoutX(20);
        re.setLayoutY(250);
        gameLayer.getChildren().add(re);


        remove.setOnMouseClicked(event -> {
            lineLayer.getChildren().clear();
            steps = new ArrayList<Integer>();


        });
    }


    public void gameInitialize(int numberPiece) {


        hg = new HexGame(numberPiece);
        game = hg.toString();
        int length = game.length();
        String Crannies = game.substring(0, 18);
        Nooks = game.substring(18, 90);
        String startpositions = game.substring(90, length);

        nook = Game.getNook(game);//without direction

        cranny = Game.getCranny(game);

        List<Integer> boundry = Block.getBoundryList();


        List<Integer> StP = new ArrayList<>();
        for (int i = 0; i < length - 90; i = i + 3) {
            StP.add(Integer.parseInt(startpositions.substring(i, i + 3)));
        }//get a list of start points

        //set Player according to game state
        for (int stp : StP) {
            drawPlayer(stp);

        }


        List<String> nooks = new ArrayList<>();
        for (int i = 0; i < 72; i = i + 4) {
            nooks.add((Nooks.substring(i, i + 4)));
        }//a list of 18 nooks


        //set nooks according to game state
        for (int i = 0; i < 18; i++) {
            drawNook(nooks.get(i));
        }


        /*set cranny according to the game state*/
        for (int i = 0; i < 6; i++) {
            drawCranny(cranny.get(i));
        }

        player = StP.get(0);
        goal = Destination.createDestination(Nooks, startpositions);
        drawGoal(goal);


        int[] bestPath = HexGame.minimalPath(game, player, goal);
        solutionList = new ArrayList<Integer>();
        for (int intValue : bestPath) {
            solutionList.add(intValue);
        }




    }


    //belowings are the drawing parts
    private class Hexagon extends Polygon {
        double x, y, side;
        int number;

        public Hexagon(double x, double y, double side, int number) {
            this.x= x;
            this.y = y;
            this.side = side;
            this.number = number;
            getPoints().addAll(
                    x + side / 2, y - Math.sqrt(3) * side / 2,
                    x + side, y,
                    x + side / 2, y + Math.sqrt(3) * side / 2,
                    x - side / 2, y + Math.sqrt(3) * side / 2,
                    x - side, y,
                    x - side / 2, y - Math.sqrt(3) * side / 2);
            setStroke(Color.LIGHTGRAY);
            setStrokeWidth(1);
            setFill(Color.rgb(255, 255, 255, 0.01));
            setRotate(90);

            Text t = new Text("" + number);
            t.setLayoutX(x - 15);
            t.setLayoutY(y);
            t.setFill(Color.LIGHTGRAY);
            t.setFont(Font.font("Tahoma", FontWeight.LIGHT, 16));
            t.setTextAlignment(TextAlignment.CENTER);
            t.setTextOrigin(VPos.CENTER);
            gameLayer.getChildren().add(t);

            this.setOnMouseClicked(event -> {
                int currentPosition;
                if (steps.size() == 0)
                    currentPosition = player;
                else
                    currentPosition = steps.get(steps.size() - 1);
                drawStep(game, currentPosition, number);
            });


        }
    }

  //drawing step when clicking the mouse
    public void drawStep(String game, int from, int to) {
        gameLayer.getChildren().remove(warning);
        if (HexGame.legitimateStep(game, from, to)) {
            steps.add(to);
            Pieces.updatePiecePosition(0, to);
            Point fp = Point.convertHex(from);
            Point tp = Point.convertHex(to);

            Line step = new Line(fp.getX() + CENTER_X, fp.getY() + CENTER_Y,
                    tp.getX() + CENTER_X, tp.getY() + CENTER_Y);
            step.setStroke(Color.BLUE);
            step.setStrokeWidth(3);
            step.toFront();
            lineLayer.getChildren().add(step);
        } else {
            warning = new Text("Not A Legitimate Step!");
            warning.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
            warning.setFill(Color.RED);
            warning.setLayoutX(scene.getWidth() / 4 + 70);
            warning.setLayoutY(70);
            gameLayer.getChildren().addAll(warning);
        }
    }

    //draw the minimal path

    private void drawSolution(ArrayList<Integer> solutionList) {
        for (int i = 0; i < solutionList.size() - 1; i++) {
            Line step = new Line(Point.convertHex(solutionList.get(i)).getX() + CENTER_X, Point.convertHex(solutionList.get(i)).getY() + CENTER_Y,
                    Point.convertHex(solutionList.get(i + 1)).getX() + CENTER_X, Point.convertHex(solutionList.get(i + 1)).getY() + CENTER_Y);

            step.setStroke(Color.YELLOW);
            step.setStrokeWidth(4);
            lineLayer.getChildren().add(step);
        }
    }

//draw destination
    private void drawGoal(int number) {
        Point p = Point.convertHex(number);
        double xPos = p.getX() + scene.getWidth() / 2;
        double yPos = p.getY() + scene.getHeight() / 2;
        Circle player = new Circle(xPos, yPos, 5, Color.RED);
        gameLayer.getChildren().add(player);

    }


    private void drawCranny(int number) {

        Point p = Point.convertHex(number);
        double xPos = p.getX() + scene.getWidth() / 2;
        double yPos = p.getY() + scene.getHeight() / 2;
        Line line = null;

        double triConst = Math.sqrt(3) * SIZE / 2;

        if (169 <= number && number <= 176) {
            line = new Line(xPos + triConst, yPos - SIZE / 2,
                    xPos + triConst, yPos + SIZE / 2);

        } else if (177 <= number && number <= 184) {
            line = new Line(xPos, yPos + SIZE,
                    xPos + triConst, yPos + SIZE / 2);

        } else if (185 <= number && number <= 192) {
            line = new Line(xPos, yPos + SIZE,
                    xPos - triConst, yPos + SIZE / 2);

        } else if (193 <= number && number <= 200) {
            line = new Line(xPos - triConst, yPos - SIZE / 2,
                    xPos - triConst, yPos + SIZE / 2);

        } else if (201 <= number && number <= 208) {
            line = new Line(xPos, yPos - SIZE,
                    xPos - triConst, yPos - SIZE / 2);

        } else if (209 <= number && number <= 216) {
            line = new Line(xPos, yPos - SIZE,
                    xPos + triConst, yPos - SIZE / 2);

        }

        //for error removing in IntelliJ
        if (line != null) {
            line.setStroke(Color.BLACK);
            line.setStrokeWidth(3);
            gameLayer.getChildren().add(line);
        }

    }

    private void drawNook(String nook) {
        int nookNo = Integer.parseInt(nook.substring(0, 3));
        Point p = Point.convertHex(nookNo);
        double xPos = p.getX() + scene.getWidth() / 2;
        double yPos = p.getY() + scene.getHeight() / 2;
        Line line1 = null;
        Line line2 = null;
        Line line3 = null;

        double triConst = Math.sqrt(3) * SIZE / 2;

        if (nook.contains("A")) {
            line1 = new Line(xPos - triConst, yPos + SIZE / 2,
                    xPos - triConst, yPos - SIZE / 2);
            line2 = new Line(xPos - triConst, yPos - SIZE / 2
                    , xPos, yPos - SIZE);
            line3 = new Line(xPos, yPos - SIZE,
                    xPos + triConst, yPos - SIZE / 2);

        } else if (nook.contains("B")) {
            line1 = new Line(xPos - triConst, yPos - SIZE / 2
                    , xPos, yPos - SIZE);
            line2 = new Line(xPos, yPos - SIZE,
                    xPos + triConst, yPos - SIZE / 2);
            line3 = new Line(xPos + triConst, yPos - SIZE / 2,
                    xPos + triConst, yPos + SIZE / 2);

        } else if (nook.contains("C")) {
            line1 = new Line(xPos, yPos - SIZE,
                    xPos + triConst, yPos - SIZE / 2);
            line2 = new Line(xPos + triConst, yPos - SIZE / 2,
                    xPos + triConst, yPos + SIZE / 2);
            line3 = new Line(xPos, yPos + SIZE,
                    xPos + triConst, yPos + SIZE / 2);

        } else if (nook.contains("D")) {
            line1 = new Line(xPos + triConst, yPos - SIZE / 2,
                    xPos + triConst, yPos + SIZE / 2);
            line2 = new Line(xPos, yPos + SIZE,
                    xPos + triConst, yPos + SIZE / 2);
            line3 = new Line(xPos, yPos + SIZE,
                    xPos - triConst, yPos + SIZE / 2);

        } else if (nook.contains("E")) {
            line1 = new Line(xPos, yPos + SIZE,
                    xPos + triConst, yPos + SIZE / 2);
            line2 = new Line(xPos, yPos + SIZE,
                    xPos - triConst, yPos + SIZE / 2);
            line3 = new Line(xPos - triConst, yPos + SIZE / 2,
                    xPos - triConst, yPos - SIZE / 2);

        } else if (nook.contains("F")) {
            line1 = new Line(xPos, yPos + SIZE,
                    xPos - triConst, yPos + SIZE / 2);
            line2 = new Line(xPos - triConst, yPos + SIZE / 2,
                    xPos - triConst, yPos - SIZE / 2);
            line3 = new Line(xPos - triConst, yPos - SIZE / 2
                    , xPos, yPos - SIZE);

        }


        if (line1 != null && line2 != null && line3 != null) {
            line1.setStroke(Color.BLACK);
            line1.setStrokeWidth(3);
            line1.toFront();
            gameLayer.getChildren().add(line1);
            line2.setStroke(Color.BLACK);
            line2.setStrokeWidth(3);
            line2.toFront();
            gameLayer.getChildren().add(line2);
            line3.setStroke(Color.BLACK);
            line3.setStrokeWidth(3);
            line3.toFront();
            gameLayer.getChildren().add(line3);


        }

    }

    private void drawPlayer(int number) {
        Point p = Point.convertHex(number);
        double xPos = p.getX() + scene.getWidth() / 2;
        double yPos = p.getY() + scene.getHeight() / 2;
        Circle player = new Circle(xPos, yPos, 10, Color.BLUE);
        player.toFront();
        gameLayer.getChildren().add(player);
    }

    private void drawBoard() {
        //set hexagon with number

        int h;//hexnumber

        for (h = 0; h < 217; h++) {
            Point p = Point.convertHex(h);
            Hexagon hex = new Hexagon(scene.getWidth() / 2 + p.getX(),
                    scene.getHeight() / 2 + p.getY(),
                    SIZE,
                    h);
            gameLayer.getChildren().add(hex);

        }
        //set outer boundry

        for (h = 169; h <= 177; h++) {
            Point p = Point.convertHex(h);
            Line lineTop1 = new Line(scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 - SIZE,
                    scene.getWidth() / 2 + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2);
            lineTop1.setStroke(Color.BLACK);
            lineTop1.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop1);

            Line lineTop2 = new Line(scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 - SIZE,
                    scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2);
            lineTop2.setStroke(Color.BLACK);
            lineTop2.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop2);


        }
        for (h = 177; h <= 185; h++) {
            Point p = Point.convertHex(h);
            Line lineTop1 = new Line(scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 - SIZE,
                    scene.getWidth() / 2 + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2);
            lineTop1.setStroke(Color.BLACK);
            lineTop1.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop1);

            Line lineTop2 = new Line(scene.getWidth() / 2 + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2,
                    scene.getWidth() / 2 + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2);
            lineTop2.setStroke(Color.BLACK);
            lineTop2.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop2);


        }
        for (h = 185; h <= 193; h++) {
            Point p = Point.convertHex(h);
            Line lineTop1 = new Line(CENTER_X + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2,
                    scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 + SIZE);
            lineTop1.setStroke(Color.BLACK);
            lineTop1.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop1);

            Line lineTop2 = new Line(scene.getWidth() / 2 + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2,
                    scene.getWidth() / 2 + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2);
            lineTop2.setStroke(Color.BLACK);
            lineTop2.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop2);


        }
        for (h = 193; h <= 201; h++) {
            Point p = Point.convertHex(h);
            Line lineTop1 = new Line(scene.getWidth() / 2 + p.getX() + Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2,
                    scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 + SIZE);
            lineTop1.setStroke(Color.BLACK);
            lineTop1.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop1);

            Line lineTop2 = new Line(scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2,
                    scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 + SIZE);
            lineTop2.setStroke(Color.BLACK);
            lineTop2.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop2);

        }
        for (h = 201; h <= 209; h++) {
            Point p = Point.convertHex(h);
            Line lineTop1 = new Line(scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2,
                    scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2);
            lineTop1.setStroke(Color.BLACK);
            lineTop1.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop1);

            Line lineTop2 = new Line(scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2,
                    scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 + SIZE);
            lineTop2.setStroke(Color.BLACK);
            lineTop2.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop2);

        }
        for (h = 209; h <= 216; h++) {
            Point p = Point.convertHex(h);
            Line lineTop1 = new Line(scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 + SIZE / 2,
                    scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2);
            lineTop1.setStroke(Color.BLACK);
            lineTop1.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop1);

            Line lineTop2 = new Line(scene.getWidth() / 2 + p.getX() - Math.sqrt(3) * SIZE / 2,
                    p.getY() + scene.getHeight() / 2 - SIZE / 2,
                    scene.getWidth() / 2 + p.getX(),
                    p.getY() + scene.getHeight() / 2 - SIZE);
            lineTop2.setStroke(Color.BLACK);
            lineTop2.setStrokeWidth(3);
            gameLayer.getChildren().add(lineTop2);

        }
        //for a line missing
        Line lineM = new Line(scene.getWidth() / 2 + Point.convertHex(169).getX() - Math.sqrt(3) * SIZE / 2,
                Point.convertHex(169).getY() + scene.getHeight() / 2 + SIZE / 2,
                scene.getWidth() / 2 + Point.convertHex(169).getX() - Math.sqrt(3) * SIZE / 2,
                Point.convertHex(169).getY() + scene.getHeight() / 2 - SIZE / 2);
        lineM.setStroke(Color.BLACK);
        lineM.setStrokeWidth(3);
        gameLayer.getChildren().add(lineM);


    }
}













