We declare that the work we have submitted for Stage C of this assignment and all stages before it is entirely our own work, with the following documented exceptions:

* The code in class <legitimate nook for seperating string and integer> is based on a solution we found when researching the problem "http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java"


Signed: Sining Li (u5793447), Ke Chen (u5506946), and Yue Pan (u5591110)
