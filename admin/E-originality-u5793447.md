I declare that the work I have submitted for Stage E of this assignment and all stages before it is entirely my own work, with the
following documented exceptions:

* The idea of <representing cell with coordinate> came from the website<http://www.redblobgames.com/grids/hexagons/#coordinates>

* The code in class <Coordinate> that converts coordinate uses an idea suggested by the website<http://www.redblobgames.com/grids/hexagons/#coordinates>

*The code in class <Nook> that uses char uses an idea suggested by the website http://stackoverflow.com/questions/6906001/how-do-i-used-a-char-as-the-case-in-a-switch-case

Signed: Sining Li (u5793447)
