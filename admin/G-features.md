Our project implements the following features.

('X' indicates implemented, '-' indicates unimplemented).
Correctly implements all of the Part One criteria.X
Appropriate use of git (as demonstrated by the history of your repo).X
Generates a correct game state when a HexGame object is created using the constructor that takes a game description.X
The game a correct random game state when a HexGame object is created using its default constructor.X
The game indicates a solvable objective to the user at the start of a round, and allows the user to input their solution via the mouse.X
The game checks that users' solution is correct.X
The game supports multiple players.X
The game identifies an optimal solution if the player's solution was not optimal.X
The game supports a timer.

Note: It's has been found that constructor may occur error in a small probability.
 -

additional features...
The game can redo the wrong step if wanted. X

