We declare that the work we have submitted for Stage G of this assignment and all stages before it is entirely our own work, with the following documented exceptions:

* The idea of using <...> to make the game run faster came from a discussion with <...> (noted in source code comments)

* The code in class <Timmer> is based on a solution we found when researching the problem (http://asgteach.com/2011/10/javafx-animation-and-binding-simple-countdown-timer-2/#Listing2)

* ....

Signed: Sining Li (u5793447), Ke Chen (u5506946), and Yue Pan (u5591110)


