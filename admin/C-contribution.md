We declare that the work toward our submission of Stage B was distributed among the group members as follows:

* u5793447 40
* u5506946 30
* u5591110 30

Signed: Sining Li (u5793447), Ke Chen (u5506946), and Yue Pan (u5591110)
